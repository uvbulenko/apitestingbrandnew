import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

//import { describe, it } from 'mocha';

const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();

describe("Posts happy path", () => {
    let accessToken: string;
    let postuserid: number;
    
    let useremail: string;
    let userpassword: string;
    let username: string;
    let postsData;

    let newpostid: number;

    //Test user login data
    useremail = "mycardog@gmail.com";
    username = "myhorse";
    userpassword = "myhorse";
   
    //Using before hook to login
    before(`Login and get the token`, async () => {  //This is start of before hook
        let response = await auth.login(useremail, userpassword);

        accessToken = response.body.token.accessToken.token;
        postuserid = response.body.user.id;

    });

    // 1) Get all Posts : GET /api/Posts
    it(`Should return the list of all Posts and check the body is not empty`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        
        //Get some info for the recent Post - For reuse in set Likes
        postsData = response.body[0];

    });    


    //Create a new Post
    it(`should create post and validate response code and createdAt`, async () => {

        let UserPostData = {
            authorId: postuserid,
            body: "Hurra were here"
        };

        let response = await posts.CreatePost(UserPostData, accessToken);

        newpostid = response.body.id; //Save NewPost ID for reuse while search test
        expect(response.body.createdAt, 'createdAt field should not be empty').to.not.be.empty;

        checkStatusCode(response, 200);
    });



    // Find newly created POST : GET /api/Posts
    it(`Should find the particular newly created post `, async () => {
        let response = await posts.getAllPosts();

        const foundObject = response.body.find(item => item.id === newpostid);
        
        // Make the assertion
        expect(foundObject, 'Object with new post ID should be found in the response').to.exist;

    });    


    //Set like to the most recent Post
    it(`should like post and validate response code`, async () => {

        let UserLikeData = {
            entityId: newpostid,
            isLike: true,
            userId: postuserid
        };
       
        let response = await posts.LikePost(UserLikeData, accessToken);
        
        checkStatusCode(response, 200);
    });


    // Get Post recent post and check if like was set
    it(`Should return like true the reaction of the recent post`, async () => {
        let response = await posts.getAllPosts();

        const firstReaction = response.body[0].reactions[0];

        expect(firstReaction.isLike, 'isLike should be true').to.equal(true);

        checkStatusCode(response, 200);

    });    


    // Set like to the most recent Post
    
    it(`should unlike post and validate response code`, async () => {

        let UserLikeData = {
            entityId: newpostid,
            isLike: false,
            userId: postuserid
        };

        let response = await posts.LikePost(UserLikeData, accessToken);
        checkStatusCode(response, 200);
    });


    // 5) Get Post recent post and check if like was set
    it(`Should return the reaction of the recent post`, async () => {
        let response = await posts.getAllPosts();

        expect(response.body[0].reactions, "Reactions section should be empty").to.be.an('array').that.is.empty;
          
    });    


});
